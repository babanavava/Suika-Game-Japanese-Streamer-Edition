# Suika-Game-Japanese-Streamer-Edition

[![](https://github.com/babanavava/Suika-Game-Japanese-Streamer-Edition/assets/91779992/e3a94cce-07a3-4de2-ab89-dcc0a0848795)](https://youtu.be/LYyDveixEjw)

I created "Suika Game: Japanese Streamer Edition" which evolves in the order in which the famous streamers who would have made the Suika Game popular in Japan delivered Suika Game.

If you have any objections, please comment.

私の好きな日本の配信者をスイカゲームにしてみました。異議があれば唱えてください。

好きな配信者
```
布□ちゃん
おおえ〇たかゆき
〇こう
サワヤ〇
たい〇
す〇〇
〇〇うた
加藤純□
莉〇
る〇と
高□健神
```

## Installation

**Note that this edition only supports Suika Game v1.0.0**

First, you need xDelta: https://www.romhacking.net/utilities/598/

and NxDumpTool: https://github.com/DarkMatterCore/nxdumptool

Use NxDumpTool to create a romFS dump of your copy of the game, then get 3 files from your dump.

"resources.assets" ; "sharedassets1.assets" ; "sharedassets3.assets"

Copy them into wherever you put your xDelta patches from the download, and patch each file from your romFS dump with the corresponding patch.

Then, we'll make a LayeredFS folder structure to have these newly patched files loaded instead of the original game files.

After that, place 3 patched files in `SD:/atmosphere/contents/0100800015926000/romfs/Data/`.

## Credits
- [blashy101](https://github.com/blashy101) / [Watermelon-Game-English-Patch](https://github.com/blashy101/Watermelon-Game-English-Patch) Without this repository, I would not have known to put it in the Data folder.

